export default [
    {
        id: "1",
        title: "Quick & Easy Payments",
        description : "Grow your business by accepting card payments with the new card number",
        image: require('./assets/images/1.png'),
    },
    {
        id: "2",
        title: "Quick & Easy Payments 2",
        description : "Grow your business by accepting card payments with the new card number",
        image: require('./assets/images/2.png'),
    },
    {
        id: "3",
        title: "Quick & Easy Payments 3",
        description : "Grow your business by accepting card payments with the new card number",
        image: require('./assets/images/3.png'),
    }
]
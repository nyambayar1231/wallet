export default [
    {
        id: "1",
        category: "Food & Drinks",
        image: require('./assets/images/1.png'),
        backgroundColor : "#B4161B"
    },
    {
        id: "2",
        category: "Shopping",
        image: require('./assets/images/2.png'),
        backgroundColor : "#F4BE2C"
    },
    {
        id: "3",
        category: "Housing",
        image: require('./assets/images/3.png'),
        backgroundColor : "#B4161B"
    },
    {
        id: "4",
        category: "Transportation",
        image: require('./assets/images/3.png'),
        backgroundColor : "#AF9D5A"
    },
    {
        id: "5",
        category: "Vehicle",
        image: require('./assets/images/3.png'),
        backgroundColor : "#FF6666"
    },
    {
        id: "6",
        category: "Life & Entertainment",
        image: require('./assets/images/3.png'),
        backgroundColor : "#03C6C7"
    },
    {
        id: "7",
        category: "Communication, PC",
        image: require('./assets/images/3.png'),
        backgroundColor : "#CAD5E2"
    },
    {
        id: "8",
        category: "Financial expenses",
        image: require('./assets/images/3.png'),
        backgroundColor : "#1C8D73"
    },
    {
        id: "9",
        category: "Investments",
        image: require('./assets/images/3.png'),
        backgroundColor : "#03203C"
    },
    {
        id: "10",
        category: "Income",
        image: require('./assets/images/3.png'),
        backgroundColor : "#242B2E"
    },
]
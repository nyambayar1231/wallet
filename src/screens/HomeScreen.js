import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
const HomeScreen = ({navigation}) => {
    const user = auth().currentUser;
    if (user) {
    console.log('User email ============>: ', user.uid);
    }
    const addFireStore = () => {
        firestore()
        .collection('Records')
        .add({
            userId: user?.uid,
            category: 'Travel',
            price: 30,
        })
        .then(() => {
            console.log('Record added');
        });
    }
    const addRecord = () => {
        navigation.navigate("AddRecord");
    }
    return (
    <View style = {styles.container}>
        <View style  = {styles.accountsContainer}>
            <View style = {styles.cardContainer} >
                <Text style = {{fontSize : 18, fontWeight : "700"}}>List of accounts</Text>
                <Text style = {{color:"blue"}}>Settings</Text>
            </View>
            <View style = {styles.cashContainer} >
                <View style = {styles.cash}>
                    <Text>Cash</Text>
                    <Text>-MNT 1.470,000.00</Text>
                </View>
                <View style = {styles.addAccount}>
                    <TouchableOpacity onPress = {() => navigation.navigate("AddAccount")}>
                          <Text>ADD ACCOUNT +</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style = {styles.adjustBalance}>
                    <Text>
                        ADJUST BALANCE
                    </Text>
                    <TouchableOpacity onPress = {addRecord} style = {{backgroundColor : "#383CC1", borderRadius : 5}}>
                        <Text>
                            + Record
                        </Text>
                    </TouchableOpacity>
            </View>
        </View>
        <View style = {styles.expansesStructureContainer}>
               <View style = {{padding :5}}>
                   <View style = {{marginBottom : 5}}>
                        <Text style = {{fontSize : 18, fontWeight : "700"}}>
                            Expenses structure
                        </Text>
                   </View>
                         <Text>
                            Last 30 DAYS
                        </Text>
                    <Text>
                        MNT 0
                    </Text>
                    <Text style = {{color: "blue",}}>
                        SHOW MORE
                    </Text>
                    <TouchableOpacity onPress = {addFireStore}>
                        <Text>
                            Add FireStore Cloud
                        </Text>
                    </TouchableOpacity>
               </View>
        </View>
    </View>
    )
}

export default HomeScreen;

const styles = StyleSheet.create({
    container : {
        flex:1, 
        backgroundColor : "#ddd"
    },
    accountsContainer : {
        backgroundColor : "#FFF",
        padding : 15
    },
    cardContainer : {
        justifyContent: "space-between",
        flexDirection : "row"
    },
    expansesStructureContainer : {
        margin: 15, 
        backgroundColor : "#FFF", 
        padding : 5, 
        borderRadius : 5
    },
    cashContainer : {
        marginTop: 15,
        flexDirection : "row", 
        justifyContent: "space-between"
    },
    cash : {
        backgroundColor : "#2827CC", 
        padding :3,width: "49%", 
        borderRadius : 5
    },
    addAccount : {
        borderWidth : 2, 
        borderColor: "blue", 
        borderRadius : 5, 
        width : "49%" , 
        justifyContent : "center", 
        alignItems : "center"
    },
    adjustBalance : {
        marginTop: 15, 
        flexDirection : "row", 
        justifyContent : "space-between"
    }
})

import React, {useEffect, useState} from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ActivityIndicator, FlatList } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import {Category} from "../components/CategoryContainer";
const Items = ({item}) => {
    return (
        <View style = {{flex: 0.9}}>
            <Category text = {item.category} backGroundColor = {item.backgroundColor ?? ""} price = {item.price} url = {require("../../assets/images/1.png")}/>
        </View>
    )
}
const RecordsScreen = () => {
    const [loading, setLoading] = useState(true); // Set loading to true on component mount
    const [users, setUsers] = useState([]); // Initial empty array of users
    console.log("==========>",users[0]);
  useEffect(() => {
    const subscriber = firestore()
      .collection('Records')
      .onSnapshot((querySnapshot ) => {
        // see next step
        const users = [];
        querySnapshot.forEach(documentSnapshot => {
            users.push({
            ...documentSnapshot.data(),
            key: documentSnapshot.id,
            });
        });
      setUsers(users);
      setLoading(false);
      });
    // Unsubscribe from events when no longer in use
    return () => subscriber();
  }, []);
  if (loading) {
    return <ActivityIndicator  color = "red" />;
  }
    return (
        <View style = {{flex:1}}>
            <View style = {{ padding : 5, backgroundColor : "#fff"}}>
                <Text style = {{fontSize : 28, textAlign : "center", fontWeight : "800"}}>
                    ALL CATEGORIES
                </Text>
            </View>
            <FlatList 
                renderItem = {Items}
                data = {users} 
            />
        </View>
    )
}

export default RecordsScreen;

const styles = StyleSheet.create({})

import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import auth from "@react-native-firebase/auth";
const ProfileScreen = () => {
    const logOut = () => {
        auth()
        .signOut()
        .then(() => console.log('User signed out!'));
    }
    return (
        <View>
            <Text></Text>
            <TouchableOpacity onPress = {logOut}>
                <Text>Log out</Text>
            </TouchableOpacity>
        </View>
    )
}

export default ProfileScreen;

const styles = StyleSheet.create({})

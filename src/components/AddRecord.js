import React , {useEffect, useState,useRef} from 'react';
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import AntDesign from "react-native-vector-icons/AntDesign";

const AddRecord = ({navigation,route}) => {
    const user = auth().currentUser;
    const [recordAmount, setRecordAmount] = useState("");
    const getRecord = (text) => {  
        setRecordAmount(text)
    }
    const addFireStore = () => {
        firestore()
        .collection('Records')
        .add({
            userId: user?.uid,
            category: route.params?.category ?? "Food & Drinks",
            price: recordAmount,
            backgroundColor : route.params?.backGroundColor ?? "red"
        })
        .then(() => {
            console.log('Record added');
            navigation.navigate("BottomTab");
        });
    }
    return (
        <View style = {styles.container}>
            <View style = {{flex: 0.1,justifyContent: "space-between",alignItems : "center", flexDirection : "row", backgroundColor : "#383CC1"}}> 
                <TouchableOpacity onPress = {() => navigation.goBack()} >
                     <AntDesign name="close" size={35} color="black"></AntDesign>
                </TouchableOpacity>
                <TouchableOpacity onPress = {addFireStore}>
                     <AntDesign name="check" size={35} color="black"></AntDesign>
                </TouchableOpacity>
            </View>
            <View style = {{flex: 1 }}>
               <View style = {{ flexDirection : "row", flex: 1, justifyContent : "space-between" , flex : 0.2}}>
                    <View style = {{flex:0.33, justifyContent: "center", alignItems : "center", }}>
                        <Text style = {{fontSize : 22, color: "#383CC1", fontWeight : "800"}}>Income</Text>
                    </View>
                    <View style = {{flex:0.33, justifyContent: "center", alignItems : "center", }}>
                        <Text style = {{fontSize : 22, color: "#383CC1", fontWeight : "800"}}>EXPENSE</Text>
                    </View>
                    <View style = {{flex:0.33, justifyContent: "center", alignItems : "center" ,}}>
                        <Text style = {{fontSize : 20, color: "#383CC1", fontWeight : "800"}}>TRANSFER</Text>
                    </View>
                </View>
                <View style = {{flex : 1, borderWidth :1, backgroundColor : route.params?.backGroundColor ?? "red" }}>
                    <View style  = {{justifyContent : 'space-around', alignItems : "center",  flexDirection : "row", flex: 1}}>
                        <Text style = {{fontSize : 48}}>- </Text>
                        <Text style = {{fontSize : 48}}>{recordAmount} </Text>
                        <Text style = {{fontSize : 48}}>MNT </Text>
                    </View>
                    <View style = {{flex: 1,flexDirection : "row", justifyContent :"space-between", alignItems : "flex-end", padding :20}}>
                         <TouchableOpacity>
                             <Text style = {{fontSize: 22 , color: "blue"}}>Account</Text>
                             <Text style = {{fontSize: 22}}>Cash</Text>
                         </TouchableOpacity>
                         <TouchableOpacity onPress = {() => navigation.navigate("Categories")}>
                             <Text style = {{color: "#323633", fontSize : 16, textAlign : "center"}}>Category</Text>
                             {<Text style = {{fontSize: 22, }}>{route.params?.category ?? "Food and drinks"}</Text>}
                         </TouchableOpacity>
                    </View>
                </View>
                <TextInput placeholder = "Enter your amount" autoFocus = {true}  keyboardType = "number-pad"  onChangeText = {(text) =>getRecord(text)}/>
            </View>
        </View>
    )
}

export default AddRecord

const styles = StyleSheet.create({
    container: {
        flex:1
    },
    header: {
        justifyContent: "space-between",
        marginHorizontal:  5,
        flexDirection : "row",
        flex: 0.2
    }
})

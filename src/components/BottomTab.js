import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from '../screens/HomeScreen';
import RecordsScreen from '../screens/RecordsScreen';
import AccountsScreen from '../screens/AccountsScreen';
import ProfileScreen from '../screens/ProfileScreen';
import AddRecord from './AddRecord';
const Tab = createBottomTabNavigator();
const BottomTab = () => {
    return (
    <Tab.Navigator >
        <Tab.Screen name = "Home" component = {HomeScreen}/>
        <Tab.Screen name = "Records" component = {RecordsScreen}/>
        <Tab.Screen name = "Accounts" component = {AccountsScreen}/>
        <Tab.Screen name = "Profile" component = {ProfileScreen}/>
    </Tab.Navigator>
    )
}
export default BottomTab

const styles = StyleSheet.create({})

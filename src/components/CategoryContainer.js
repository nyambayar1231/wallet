import React from "react";
import {TouchableOpacity, View, Image, Text, StyleSheet} from "react-native";
export const Category = ({text, url, backGroundColor, price})=>{
    return(
        <View style={styles.category} >
            <View style={[styles.CategoryImage, {backgroundColor:backGroundColor}]}>
                <Image source={url} style={{width:25, height:25}}></Image>
            </View>
            <View style = {{flexDirection : "row", justifyContent : "space-between", flex: 1, paddingRight : 25}}>
                <Text style={styles.CategoryText}>{text}</Text>
                <Text style = {[styles.CategoryText, {color : "green"}]}>MNT {price}</Text>
            </View>
        </View>

    )
}
const styles = StyleSheet.create({
    CategoryText:{
        fontSize:18,
        fontWeight:'600',
        paddingLeft:25,
    },
    CategoryImage:{
        width: 50,
        height: 50,
        borderRadius: 50 /2,
        justifyContent:'center',
        alignItems:'center'
    },
    category:{
        borderWidth:2,
        borderColor:'#e6e6e6',
        flexDirection:'row',
        padding:8,
        alignItems:'center',
        backgroundColor:'white'
    },
})
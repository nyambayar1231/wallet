import React from 'react'
import { FlatList, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import {Category} from "../components/CategoryContainer";
import categories from "../../categories";
const Categories = ({navigation}) => {
    return (
        <ScrollView>
            <View style = {{padding : 15}}>
                <Text style = {{fontSize : 24}}> ALL CATEGORIES</Text>
            </View>
           {categories.map(el => {
               return (
                   <TouchableOpacity key = {el.id} onPress = {() => {navigation.navigate({
                       name: "AddRecord",
                       params: {category: el.category, backGroundColor : el.backgroundColor}
                   })}}>
                        <Category text = {el.category} backGroundColor = {el.backgroundColor}/>
                   </TouchableOpacity>
               )
           })}
        </ScrollView>
    )
}

export default Categories

const styles = StyleSheet.create({})

import React, {useState} from "react";
import {View, TouchableOpacity, TextInput,Text} from "react-native"
import AntDesign from "react-native-vector-icons/AntDesign";
import {Picker} from '@react-native-picker/picker';
const AddAccount = ({navigation}) => {
    const [selectedLanguage, setSelectedLanguage] = useState();
    return (
        <View style = {{flex: 1}}>
            <View style = {{flex: 0.2,justifyContent: "space-between",alignItems : "center", flexDirection : "row", backgroundColor : "#383CC1"}}> 
                <TouchableOpacity onPress = {() => navigation.goBack()} >
                        <AntDesign name="close" size={35} color="white"></AntDesign>
                </TouchableOpacity>
                <Text style = {{fontSize : 24, color: "white"}}>ADD YOUR ACCOUNT</Text>
                <TouchableOpacity>
                        <AntDesign name="check" size={35} color="white"></AntDesign>
                </TouchableOpacity>
            </View>
            <View style = {{justifyContent : "center", flex : 0.7}}>
                <Text style = {{left: 5, color: "grey"}}>Account name</Text>
                <TextInput style = {{borderBottomWidth : 2, borderColor : "blue", fontSize : 18, padding :10}}/>
                <Text style = {{left: 5, color: "grey"}}>Initial value</Text>
                <TextInput style = {{borderBottomWidth : 2, borderColor : "blue", fontSize : 18, padding :10 }}/>
                <View style = {{top : 10}}>
                    <Text>Currency</Text>
                    <Picker
                        selectedValue={selectedLanguage}
                        onValueChange={(itemValue, itemIndex) =>
                        setSelectedLanguage(itemValue)
                    }>
                        <Picker.Item label="MNT" value="MNT" />
                        <Picker.Item label="USD" value="USD" />
                        <Picker.Item label="CHY" value="CHY" />
                    </Picker>
                </View>
            </View>
        </View>
    )
}
export default AddAccount;
import React , {useEffect, useRef} from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Animated, Button, Pressable } from 'react-native';
import Svg, {G, Circle} from 'react-native-svg';
const NextButton = ({percentage, scrollTo}) => {
    const size = 128;
    const strokeWidth = 2;
    const center = size/2;
    const radius = size / 2 - strokeWidth/2;
    const circumreference = 2 * Math.PI * radius;
    const progressAnimation = useRef(new Animated.Value(0)).current;
    const progressRef = useRef(null);

    const animation = (toValue) => {
        return Animated.timing(progressAnimation, {
            toValue,
            duration : 250,
            useNativeDriver : true
        }).start()
    }
    useEffect(() => {
        progressAnimation.addListener((value) => {
            const strokeDashoffset = circumreference - (circumreference * value.value) / 100 ;
            if (progressRef?.current) {
                progressRef.current.setNativeProps({
                    strokeDashoffset,
                })
            }
            },
            [percentage]
        );
            return () => {
                progressAnimation.removeAllListeners()
            }
    },[]);

    
    useEffect(() => {
        animation(percentage);
    },[percentage])


    return (
        <View style = {styles.container}>
            <Svg width = {size} height = {size}>
                <G rotation = "-90" origin = {center}>
                    <Circle stroke = "#E6E7E8" cx = {center} cy = {center} r= {radius} strokeWidth = {strokeWidth}
                    />
                    <Circle
                        strokeDasharray = {circumreference}
                        stroke = "#F4338F"
                        cx = {center}
                        cy = {center}
                        r = {radius}
                        strokeWidth = {strokeWidth}

                        ref={progressRef}
                    />
                  
                </G>
            </Svg>
            <TouchableOpacity style = {styles.button} activeOpacity = {0.6} onPress = {scrollTo}>
               <Text >next</Text>
            </TouchableOpacity>            
        </View>
    )
}

export default NextButton

const styles = StyleSheet.create({
    container : {
        flex:1,
        justifyContent: "center",
        alignItems : "center"
    },
    button : {
        position : "absolute",
        backgroundColor : "#f4338f",
        borderRadius : 100,
        padding : 20,
    }
})

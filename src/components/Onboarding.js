import React , {useState, useRef}from 'react';
import { FlatList, StyleSheet, Text, View, Animated  } from 'react-native';
import AsyncStorage from "@react-native-async-storage/async-storage";
import OnboardingItem from "../components/OnboardingItem";
import Paginator from "../components/Paginator";
import NextButton from "../components/NextButton";
import slides from "../../slides";
const OnBoarding = ({navigation}) => {
    const [currentIndex, setCurrentIndex] = useState(0);
    const scrollX = useRef(new Animated.Value(0)).current;
    const slidesRef = useRef(null);
    const viewableItemsChanged = useRef(({viewableItems}) => {
        setCurrentIndex(viewableItems[0].index);
    }).current;
    const viewConfig = useRef({viewAreaCoveragePercentThreshold: 50}).current;

    const scrollTo =  () => {
        if (currentIndex < slides.length - 1) {
            slidesRef.current.scrollToIndex({index : currentIndex + 1});
        } else {
            navigation.navigate("LoginHandler");
        }
    }
    return (
        <View style = {styles.container}>
            <View style = {{flex: 3}}>
                <FlatList 
                    showsHorizontalScrollIndicator={false}
                    bounces = {false} 
                    pagingEnabled  
                    data = {slides} 
                    renderItem = {({item}) => <OnboardingItem item = {item}/>} 
                    horizontal
                    keyExtractor = {(item) => item.id}
                    onScroll = {Animated.event([{nativeEvent : {contentOffset : { x: scrollX } } }], {
                        useNativeDriver : false,
                    })}
                    onViewableItemsChanged = {viewableItemsChanged}
                    viewabilityConfig = {viewConfig}
                    scrollEventThrottle = {32}
                    ref = {slidesRef}
                />
            </View>
            <Paginator data = {slides} scrollX = {scrollX} />
            <NextButton scrollTo = {scrollTo} percentage = {(currentIndex + 1) * (100 / slides.length)}/>
        </View>
    )
}

export default OnBoarding

const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent: "center",
        alignItems : "center"
    }
})

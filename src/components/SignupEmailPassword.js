import React, {useEffect,useState} from 'react'
import { StyleSheet, Text, TouchableOpacity, View, TextInput } from 'react-native'
import auth from '@react-native-firebase/auth';
const SignupEmailPassword = ({navigation}) => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const signupEmailPassword = () => {
        auth()
        .createUserWithEmailAndPassword(email, password)
        .then(() => {
            console.log('User account created & signed in!');
            navigation.navigate("LoginHandler");
    
        })
        .catch(error => {
            if (error.code === 'auth/email-already-in-use') {
            console.log('That email address is already in use!');
            }

            if (error.code === 'auth/invalid-email') {
            console.log('That email address is invalid!');
            }
            console.error(error);
        });
    }
    return (
            <View >
                <Text>Create a new account </Text>
                <TextInput placeholder = "Email" style = {{borderWidth : 1}} onChangeText = {setEmail} />
                <TextInput placeholder = "Password" style = {{borderWidth : 1}} onChangeText = {setPassword} />
                <TouchableOpacity onPress = {signupEmailPassword} style = {{justifyContent : "center", alignItems : "center"}}>
                    <Text>SignUp</Text>
                </TouchableOpacity>   
            </View>
    )
}

export default SignupEmailPassword

const styles = StyleSheet.create({
    container : {
        flex:1,
        justifyContent: 'center',
        alignItems : "center"
    }
})

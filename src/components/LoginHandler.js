import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import auth from '@react-native-firebase/auth';
import BottomTab from './BottomTab';
const LoginHandler = ({navigation}) => {
    const [initializing, setInitializing] = useState(true);
    const [user, setUser] = useState();
    // Handle user state changes
    function onAuthStateChanged(user) {
      setUser(user);
      if (initializing) setInitializing(false);
    }
    useEffect(() => {
      const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
      return subscriber; // unsubscribe on unmount
    },[]);
    if (initializing) return null;
    if (!user) {
      const signupEmailPassword = () => {
      navigation.navigate("SignupEmailPassword");
      }
      return (
        <View style = {styles.container}> 
            <TouchableOpacity>
                <Text>Login With Google</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress = {signupEmailPassword} >
                <Text>Signup With Email And Password</Text>
            </TouchableOpacity>
        </View>
      );
    }
    
    return (
      <BottomTab />
    )

}
export default LoginHandler

const styles = StyleSheet.create({
    container : {
        flex:1,
        justifyContent : "center",
        alignItems : "center"
    }
})

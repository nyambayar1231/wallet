import React from "react";
import {Image, View, TouchableOpacity} from "react-native";
import {NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from '@react-navigation/stack';
import AntDesign from "react-native-vector-icons/AntDesign";
import auth from '@react-native-firebase/auth';
import HomeScreen from "../screens/HomeScreen";
import LoginHandler from "../components/LoginHandler";
import SignupEmailPassword from "../components/SignupEmailPassword";
import Categories from "../components/Categories";
import AddRecord from "../components/AddRecord";
import OnBoarding from "../components/Onboarding";
import NextButton from "../components/NextButton";
import BottomTab from "../components/BottomTab";
import AddAccount from "../components/AddAccount";
const user = auth().currentUser;
  if (user) {
  console.log('User email ============>: ', user.uid);
  }

const Stack = createStackNavigator();
const Navigation = () => {
    return (
            <NavigationContainer>
                <Stack.Navigator initialRouteName = "OnBoarding" headerMode  = {false}>
                    <Stack.Screen name = "Home" component = {HomeScreen} />
                    <Stack.Screen name = "LoginHandler" component = {LoginHandler} options = {{headerShown: false}}/>
                    <Stack.Screen name = "SignupEmailPassword" component = {SignupEmailPassword} />
                    <Stack.Screen name = "Categories" component = {Categories} />
                    <Stack.Screen name = "AddRecord" component = {AddRecord} options={{ headerShown : false}}/>
                    <Stack.Screen name = "OnBoarding" component = {OnBoarding} />
                    <Stack.Screen name = "NextButton" component = {NextButton} />
                    <Stack.Screen name = "BottomTab" component = {BottomTab} />
                    <Stack.Screen name = "AddAccount" component = {AddAccount} />
                </Stack.Navigator>
            </NavigationContainer>
    )
}
export default Navigation;